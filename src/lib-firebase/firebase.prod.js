import Firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
// import { seedDatabase } from '../seed';
const config = {
    apiKey: "AIzaSyBn665pcnGMrBrHVLvtqb9yyqoU40tWISM",
    authDomain: "react-netflix-65177.firebaseapp.com",
    databaseURL: "https://react-netflix-65177.firebaseio.com",
    projectId: "react-netflix-65177",
    storageBucket: "react-netflix-65177.appspot.com",
    messagingSenderId: "848939130561",
    appId: "1:848939130561:web:0129818f7032e11edf7e40"
};


const firebase = Firebase.initializeApp(config);
// seedDatabase(firebase);
export { firebase };