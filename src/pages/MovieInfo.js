import React from "react";
import { Player } from "../components";
import MovieContainer from "../components/movie/MovieContainer";
import FooterContainer from "../containers/FooterContainer";
import * as ROUTES from "../routes/Routes";
const MovieInfo = (props) => {
  const sessionStorageDATA = JSON.parse(sessionStorage.getItem("movieTitle"));
  return (
    <>
      <MovieContainer.Header>
        <MovieContainer.Brand to={ROUTES.BROWSE} >Movie Database</MovieContainer.Brand>
        <MovieContainer.Title>
          Watch anywhere. Cancel anytime.
        </MovieContainer.Title>
      </MovieContainer.Header>

      {sessionStorageDATA.map((item) => (
        <div
          className="backdrop"
          key={item.title}
          style={{ background: `url(${item.backdrop})` }}
        >
          <div className="movie-poster">
            <img className="img" src={item.img} alt="alt"></img>
            <Player>
              <Player.Button />
              <Player.Video src="/videos/bunny.mp4"></Player.Video>
            </Player>
          </div>
          <div className="info-container">
            <h4>{item.title}</h4>
            <p>{item.relaseDate}</p>
            <h5 className="overview">{item.overview}</h5>
          </div>
        </div>
      ))}
      <FooterContainer></FooterContainer>
    </>
  );
};

export default MovieInfo;
