import React, { useState, useContext } from "react";
import { Form } from "../components";
import { FirebaseContext } from "../context/firebase";
import { useHistory } from "react-router-dom";
import * as ROUTES from "../routes/Routes";
import Header from "../components/header/Header";
const Signin = () => {
  const { firebase } = useContext(FirebaseContext);
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");

  function submitHendler(e) {
    e.preventDefault();
    return firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((data) => {
        localStorage.setItem("users", data.user.email);
        history.push(ROUTES.BROWSE);
      })
      .catch((error) => {
        setPassword("");
        setEmail("");
        setError(error.message);
      });
  }
  return (
    <Form.Page>
      <Header/>
      <Form>
        <h4>sign in</h4>
        {error && <Form.Error>{error}</Form.Error>}
        <Form.FormSign onSubmit={submitHendler}>
          <p>email</p>
          <Form.Input
            value={email}
            onChange={({ target }) => setEmail(target.value)}
            type="email"
          ></Form.Input>
          <p>password</p>
          <Form.Password
            value={password}
            onChange={({ target }) => setPassword(target.value)}
            type="password"
            autoComplate="off"
          ></Form.Password>
          <Form.ButtonLink>Sign In</Form.ButtonLink>
          <small>hesabın varsa giriş için</small>
          <Form.Link to={ROUTES.LOGİN}>Login</Form.Link>
        </Form.FormSign>
      </Form>
    </Form.Page>
  );
};

export default Signin;
