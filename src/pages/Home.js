import React from 'react'
import FooterContainer from '../containers/FooterContainer'
import Movies from '../containers/Movies'

const Home = () => {
    return (
        <>
          <Movies></Movies>
          <FooterContainer></FooterContainer>
        </>
    )
}

export default Home
