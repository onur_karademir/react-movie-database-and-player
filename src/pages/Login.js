import React, { useState, useContext } from "react";
import { Form } from "../components";
import * as ROUTES from "../routes/Routes";
import { FirebaseContext } from "../context/firebase";
import { useHistory } from "react-router-dom";
import Header from "../components/header/Header";
const Login = () => {
  const history = useHistory();
  const { firebase } = useContext(FirebaseContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  function loginHendler(e) {
    e.preventDefault();
    return firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((data) => {
        console.log(data.user.email);
        localStorage.setItem("users", data.user.email);
        history.push(ROUTES.BROWSE);
      })
      .catch((error) => {
        setPassword("");
        setEmail("");
        setError(error.message);
      });
  }
  return (
    <Form.Page>
      <Header/>
      <Form>
        <h4>login</h4>
        {error && <Form.Error>{error}</Form.Error>}
        <Form.FormSign onSubmit={loginHendler}>
          <p>email</p>
          <Form.Input
            value={email}
            onChange={({ target }) => setEmail(target.value)}
            type="email"
          ></Form.Input>
          <p>password</p>
          <Form.Password
            value={password}
            onChange={({ target }) => setPassword(target.value)}
            type="password"
            autoComplate="off"
          ></Form.Password>
          <Form.ButtonLink>Sign In</Form.ButtonLink>
          <small>hesabın yoksa kayıt için</small>
          <Form.Link to={ROUTES.HOME}>Sign in</Form.Link>
        </Form.FormSign>
      </Form>
    </Form.Page>
  );
};

export default Login;
