export { default as MovieContainer } from "./movie/MovieContainer";
export { default as Player } from "./player/Player";
export { default as Footer } from "./footer/Footer";
export { default as Form } from "./form/Form";
