import React, { useState, createContext, useContext } from "react";
import ReactDOM from "react-dom";
import { Container, Overlay, Button, Inner } from "./Player.El";

const PlayerContext = createContext();

const Player = ({ children, ...restProps }) => {
  const [showPlayer, setShowPlayer] = useState(false);

  return (
    <PlayerContext.Provider value={{ showPlayer, setShowPlayer }}>
      <Container {...restProps}>{children}</Container>
    </PlayerContext.Provider>
  );
};

Player.Container = function PlayerContainer({ children, ...restProps }) {
  return <Container {...restProps}>{children}</Container>;
};
Player.Video = function PlayerVideo({ src, ...restProps }) {
  const { showPlayer, setShowPlayer } = useContext(PlayerContext);

  return showPlayer
    ? ReactDOM.createPortal(
        <Overlay onClick={() => setShowPlayer(false)} data-testid="player">
          <Inner>
            <video id="movie-database" controls autoPlay loop>
              <source src={src} type="video/mp4"></source>
            </video>
          </Inner>
        </Overlay>,
        document.body
      )
    : null;
};
Player.Button = function PlayerButton({ ...restProps }) {
  const { showPlayer, setShowPlayer } = useContext(PlayerContext);

  return (
    <Button onClick={() => setShowPlayer(!showPlayer)} {...restProps}>
      Play
    </Button>
  );
};
export default Player;
