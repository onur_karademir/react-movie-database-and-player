import styled from "styled-components/macro";
import { Link as ReactRouterLink } from "react-router-dom";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: rgba(0, 0, 0, 0.75);
  border-radius: 5px;
  box-sizing: border-box;
  width: 100%;
  margin: auto;
  max-width: 450px;
  padding: 60px 68px 40px;
`;
export const Error = styled.div`
  background: #e87c03;
  border-radius: 4px;
  font-size: 14px;
  margin: 0 0 16px;
  color: white;
  padding: 15px 20px;
`;
export const Inner = styled.div``;
export const FormSign = styled.form`
  display: flex;
  flex-direction: column;
  max-width: 450px;
  width: 100%;
`;
export const Input = styled.input`
  height: 50px;
  padding: 5px 20px;
  background-color: #333;
  border-radius: 4px;
  color: #eee;
  line-height: 50px;
  margin-bottom:5px;
`;

export const Password = styled.input`
  height: 50px;
  padding: 5px 20px;
  background-color: #333;
  border-radius: 4px;
  color: #eee;
`;
export const Link = styled(ReactRouterLink)`
  text-decoration: none;
  color: #eee;
  :hover {
    color: #ffff;
    text-decoration: underline;
    font-weight:700;
  }
`;

export const ButtonLink = styled.button`
  background: red;
  color: #eee;
  font-weight: 600;
  padding: 10px 25px;
  outline: none;
  border: none;
  border-radius: 4px;
  width: 120px;
  margin: 10px auto;
  transition: all 0.5s ease-in-out;
  @media screen and (max-width: 535px) {
    padding: 5px 10px;
  }
  &:hover {
    text-decoration: none;
    color: #eee;
  }
  &:disabled {
    opacity: 0.5;
  }
`;
export const Page = styled.div`
  background: #37373a;
  height: 100vh;
  width: 100vw;
`;
