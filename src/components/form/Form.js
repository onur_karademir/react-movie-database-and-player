import React from "react";
import { Container, Inner, FormSign, Input, Password,ButtonLink,Error,Link,Page } from "./Form.El";
const Form = ({ children, ...restProps }) => {
  return <Container {...restProps}>{children}</Container>;
};

Form.Inner = function FormInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
Form.Page = function FormPage({ children, ...restProps }) {
  return <Page {...restProps}>{children}</Page>;
};
Form.Error = function FormError({ children, ...restProps }) {
    return <Error {...restProps}>{children}</Error>;
  };
Form.FormSign = function FormFormSign({ children, ...restProps }) {
  return <FormSign {...restProps}>{children}</FormSign>;
};
Form.Input = function FormInput({ children, ...restProps }) {
  return <Input {...restProps}>{children}</Input>;
};
Form.Password = function FormPassword({ children, ...restProps }) {
  return <Password {...restProps}>{children}</Password>;
};
Form.ButtonLink = function FormButtonLink({children, ...restProps }) {
  return <ButtonLink {...restProps}>{children}</ButtonLink>;
};
Form.Link = function FormLink({to,children, ...restProps }) {
  return <Link to={to} {...restProps}>{children}</Link>;
};

export default Form;
