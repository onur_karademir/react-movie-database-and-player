import styled from "styled-components/macro";
import { Col, Row} from "reactstrap";

export const MyContainer = styled.div`
  border-top: 8px solid #222;
  background-color: black;
  width: 100%;
  padding: 50px 5%;
  @media screen and (max-width: 600px) {
    padding: 30px 3%;
  }
`;
export const Column = styled(Col)``;

export const Inner = styled(Row)`
  max-width: 1000px;
  width: 100%;
  margin: 0 auto;
  text-align: center;
`;
export const Ul = styled.div``;
export const Li = styled.div`
  cursor: pointer;
  list-style: none;
  text-align:left !important;
  color: #aba3a3 !important;
  font-size: 20px;
  font-weight: 600px;
  padding: 4px;
  margin: 10px 0;
  transition: all 0.3s ease-in-out;
  text-transform: capitalize;
  position: relative;
  &:hover::after {
    content: "";
    position: absolute;
    width: 70%;
    height: 0;
    left: 0;
    text-align:left;
    bottom: 0px;
    border-bottom: 3px solid #222;
  }
  &:hover {
    color: #fff !important;
  }
  @media screen and (max-width: 600px) {
    font-size: 10px;
  }
`;
