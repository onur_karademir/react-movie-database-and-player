import React from "react";
import { MyContainer, Column, Inner, Ul, Li } from "./Footer.El";
const Footer = ({ children, ...restProps }) => {
  return (
    <MyContainer {...restProps}>
      <Inner>{children}</Inner>
    </MyContainer>
  );
};

Footer.Column = function FooterColumn({ children, ...restProps }) {
  return <Column {...restProps}>{children}</Column>;
};
Footer.Inner = function FooterInner({ children, ...restProps }) {
  return <Inner {...restProps}>{children}</Inner>;
};
Footer.Ul = function FooterUl({ children, ...restProps }) {
  return <Ul {...restProps}>{children}</Ul>;
};
Footer.Li = function FooterColumn({ children, ...restProps }) {
  return <Li {...restProps}>{children}</Li>;
};

export default Footer;
