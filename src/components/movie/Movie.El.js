import styled from "styled-components/macro";
import { Link as ReactRouterLink } from "react-router-dom";

export const Container = styled.div`
  max-width: 1500px;
  width: 100%;
  text-align: center;
  background-color: black;
`;
export const Header = styled.div`
  background-color: #222;
  width: 100%;
  height: 100px;
  display: flex;
  justify-content: space-around;
  align-items: center;
  margin: auto;
  position:relative;
`;
export const Inner = styled.div`
  width: 100%;
  height: 100%;
  margin: 0 !important;
  padding: 0 !important;
  overflow: hidden;
  position: relative;
`;
export const Info = styled.div`
  width: 100%;
  height: 80px;
  margin: 0 !important;
  padding: 0 !important;
  overflow: hidden;
  position: absolute;
  bottom: 0;
  left: 0;
  background-color: #a70b20;
  transition: all 0.5s ease-in-out;
  display: flex;
  justify-content: space-between;
  padding: 0.5rem 1rem 1rem !important;
  letter-spacing: 0.5px !important;
`;

export const Link = styled(ReactRouterLink)`
  width: 300px;
  height: 400px;
  display: inline-block;
  margin: 10px 10px;
  border: 5px #3c3a3a solid;
  transition: all 0.5s ease-in-out;
  overflow: hidden;
  &:hover ${Info} {
    background-color: #e8112e;
  }
  &:hover {
    border: 5px #eee solid;
  }
`;
export const Logout = styled(ReactRouterLink)`
  background: red;
  color: #eee;
  font-weight: 600;
  font-size:13px;
  padding: 2px 15px;
  outline: none;
  border: none;
  border-radius: 4px;
  text-decoration: none;
  position:absolute;
  bottom:0;
  right:0;
  @media screen and (max-width: 535px) {
    padding: 5px 10px;
  }
  &:hover {
    color: #eee;
    text-decoration: none;
  }
`;
export const Brand = styled(ReactRouterLink)`
  background-color: #e8112e;
  padding: 20px;
  text-decoration: none;
  color: #eee;
  font-size: 20px;
  font-weight: 700;
  border-radius: 5px;
  transition: all 0.5s ease-in-out;
  &:hover {
    transform: scale(1.05);
    color: #eee;
    text-decoration: none;
  }
  @media screen and (max-width: 535px) {
    padding: 10px;
    font-size: 14px;
  }
`;
export const Img = styled.img`
  width: 100%;
  height: 100%;
  transition: all 0.5s ease-in-out;
  &:hover {
    transform: scale(1.1);
  }
`;
export const Form = styled.form``;
export const Input = styled.input`
  padding: 10px;
  outline: none;
  border: none;
  border-radius: 4px;
  font-weight: 600;
  margin: 0 10px;
  @media screen and (max-width: 535px) {
    padding: 5px;
    width: 50%;
  }
`;
export const SubmitBtn = styled.button`
  background: red;
  color: #eee;
  font-weight: 600;
  padding: 10px 25px;
  outline: none;
  border: none;
  border-radius: 4px;
  transition: all 0.5s ease-in-out;
  @media screen and (max-width: 535px) {
    padding: 5px 10px;
  }
  &:hover {
    transform: scale(1.08);
  }
`;
export const Title = styled.p`
  color: #eee;
  font-weight: bold;
  font-size: 20px;
  @media screen and (max-width: 900px) {
    font-size: 12px;
  }
`;
export const Vote = styled.p`
  color: #eee;
  background-color: #22254b;
  border-radius: 3px;
  font-weight: bold;
  padding: 0.25rem 0.5rem;
`;
