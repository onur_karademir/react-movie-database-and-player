import React from "react";
import { Container,Header,Brand, Link, Inner, Info, Img, Form, Input, SubmitBtn , Title,Vote,Logout} from "./Movie.El";

const MovieContainer = ({ children, ...restProps }) => {
  return (
    <Link {...restProps}>
      <Inner>{children}</Inner>
    </Link>
  );
};
MovieContainer.Container = function MovieContainerContainer ({ children, ...restProps }) {
return <Container {...restProps}>{children}</Container>
}
MovieContainer.Header = function MovieContainerHeader ({ children, ...restProps }) {
return <Header {...restProps}>{children}</Header>
}
MovieContainer.Form = function MovieContainerForm ({ children, ...restProps }) {
return <Form {...restProps}>{children}</Form>
}
MovieContainer.Input = function MovieContainerInput ({ children, ...restProps }) {
return <Input {...restProps}>{children}</Input>
}
MovieContainer.SubmitBtn = function MovieContainerSubmitBtn ({ children, ...restProps }) {
return <SubmitBtn {...restProps}>{children}</SubmitBtn>
}
MovieContainer.Link = function MovieContainerLink ({ to, children, ...restProps }) {
return <Link to={to} {...restProps}>{children}</Link>
}
MovieContainer.Brand = function MovieContainerBrand ({ to, children, ...restProps }) {
return <Brand to={to} {...restProps}>{children}</Brand>
}
MovieContainer.Inner = function MovieContainerInner ({ children, ...restProps }) {
return <Inner {...restProps}>{children}</Inner>
}
MovieContainer.Info = function MovieContainerInfo ({ children, ...restProps }) {
return <Info {...restProps}>{children}</Info>
}
MovieContainer.Vote = function MovieContainerVote ({ children, ...restProps }) {
return <Vote {...restProps}>{children}</Vote>
}
MovieContainer.Img = function MovieContainerImg ({ children, ...restProps }) {
return <Img {...restProps} />
}
MovieContainer.Title = function MovieContainerTitle ({ children, ...restProps }) {
return <Title {...restProps}>{children}</Title>
}
MovieContainer.Logout = function MovieContainerLogout ({to,children, ...restProps }) {
return <Logout to={to} {...restProps}>{children}</Logout>
}

export default MovieContainer;

