import React, { Component } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavbarText,
  Container,
} from "reactstrap";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }
  render() {
    const toggle = () => {
      this.setState({
        isOpen: !this.state.isOpen,
      });
    };
    return (
      <div>
        <Navbar color="dark" light expand="md" className="mx-auto custom-style">
          <Container>
            <NavbarBrand className="brand" href="/">
              Movie App
            </NavbarBrand>
            <NavbarToggler className="custom-toggler" onClick={toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="mr-auto" navbar></Nav>
              <NavbarText className="text-white font-weight-bold font-italic">Watch anywhere. Cancel anytime. Plese to reach the content register or login.</NavbarText>
            </Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }
}

export default Header;
