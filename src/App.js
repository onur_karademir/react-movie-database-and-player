import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import * as ROUTES from "./routes/Routes";
import Home from "./pages/Home";
import MovieInfo from "./pages/MovieInfo";
import Signin from "./pages/Signin";
import Login from "./pages/Login";

function App() {
  return (
    <Router>
      <Route exact path={ROUTES.HOME}>
        <Signin />
      </Route>
      <Route exact path={ROUTES.LOGİN}>
        <Login />
      </Route>
      <Route exact path={ROUTES.MOVİE_INFO}>
        <MovieInfo />
      </Route>
      <Route exact path={ROUTES.BROWSE}>
        <Home></Home>
      </Route>
    </Router>
  );
}

export default App;
