import React from "react";
import { Footer } from "../components";

const FooterContainer = () => {
  return (
    <Footer>
      <Footer.Column md="4" sm="4" xs="4">
        <Footer.Ul>
          <Footer.Li>Center Account Media</Footer.Li>
          <Footer.Li>Jobs Redeem</Footer.Li>
          <Footer.Li>WatchTerms</Footer.Li>
          <Footer.Li>Gift</Footer.Li>
        </Footer.Ul>
      </Footer.Column>
      <Footer.Column md="4" sm="4" xs="4">
        <Footer.Ul>
          <Footer.Li>FAQ</Footer.Li>
          <Footer.Li>Investor Relations</Footer.Li>
          <Footer.Li>Contact</Footer.Li>
          <Footer.Li>Use Privacy Cookie</Footer.Li>
        </Footer.Ul>
      </Footer.Column>
      <Footer.Column md="4" sm="4" xs="4">
        <Footer.Ul>
          <Footer.Li>Help</Footer.Li>
          <Footer.Li>TestLegal Notices</Footer.Li>
          <Footer.Li>Originals</Footer.Li>
          <Footer.Li>Gift CardsBuy</Footer.Li>
        </Footer.Ul>
      </Footer.Column>
    </Footer>
  );
};

export default FooterContainer;
