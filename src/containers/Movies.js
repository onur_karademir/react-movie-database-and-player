import React, { useState, useEffect, useContext } from "react";
import { FirebaseContext } from "../context/firebase";
import MovieContainer from "../components/movie/MovieContainer";
import * as ROUTES from "../routes/Routes";
import { useHistory } from "react-router-dom";
const Movies = () => {
  const user = localStorage.getItem("users");
  console.log(user);
  const { firebase } = useContext(FirebaseContext);
  const history = useHistory();
  const APIURL =
    "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=04c35731a5ee918f014970082a0088b1&page=1";
  const IMGPATH = "https://image.tmdb.org/t/p/w1280";
  const SEARCHAPI =
    "https://api.themoviedb.org/3/search/movie?&api_key=04c35731a5ee918f014970082a0088b1&query=";

  async function getMovies(url) {
    const response = await fetch(url);
    const data = await response.json();
    console.log(data.results);
    setMovies(data.results);
  }
  useEffect(() => {
    getMovies(APIURL);
  }, []);
  const [movies, setMovies] = useState([]);
  const [input, setInput] = useState("");
  let movieArray = [];

  function submitHendler(e) {
    e.preventDefault();
    getMovies(SEARCHAPI + input);
    console.log(input);
    setInput("");
  }
  function inputHendler(e) {
    setInput(e.target.value);
  }
  function setData(item) {
    movieArray.push({
      img: `${IMGPATH + item.poster_path}`,
      title: `${item.original_title}`,
      overview: `${item.overview}`,
      relaseDate: `${item.release_date}`,
      backdrop: `${IMGPATH + item.backdrop_path}`,
    });
    console.log(movieArray);
    sessionStorage.setItem("movieTitle", JSON.stringify(movieArray));
  }

  function logOutHendler(e) {
    e.preventDefault();
    return firebase
      .auth()
      .signOut()
      .then(() => {
        history.push(ROUTES.HOME);
        localStorage.setItem("users", "");
      });
  }
  const isInvalid = input === "" || movies === "";
  return (
    <MovieContainer.Container>
      <MovieContainer.Header>
        <MovieContainer.Brand to="/browse">Movie Database</MovieContainer.Brand>
        <MovieContainer.Form onSubmit={submitHendler}>
          <MovieContainer.Input
            onChange={inputHendler}
            value={input}
          ></MovieContainer.Input>
          <MovieContainer.SubmitBtn disabled={isInvalid}>
            Search
          </MovieContainer.SubmitBtn>
        </MovieContainer.Form>
        <MovieContainer.Logout onClick={logOutHendler} to="/">
          Logout
        </MovieContainer.Logout>
        <small className="user-class">Welcome : {user}</small>
      </MovieContainer.Header>
      {movies.map((item) => (
        <MovieContainer
          key={item.id}
          onClick={() => setData(item)}
          to="/movies"
        >
          <MovieContainer.Img src={IMGPATH + item.poster_path} />
          <MovieContainer.Info>
            <MovieContainer.Title>{item.original_title}</MovieContainer.Title>
            <MovieContainer.Vote>{item.vote_average}</MovieContainer.Vote>
          </MovieContainer.Info>
        </MovieContainer>
      ))}
    </MovieContainer.Container>
  );
};

export default Movies;
